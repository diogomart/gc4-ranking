#!/usr/bin/env python

import numpy as np
import pandas

with open('FILT') as f:
    lines = f.readlines()

names_dict = {
    'core': 'A',
    'amide': 'B',
    'amine': 'C',
    'benzoval': 'D',
    'ringamide': 'E',
    'amideamide': 'F',
    'backring': 'G'
}

names_list = [
    'core', 'amide', 'amine', 'benzoval', 'ringamide',
    'amideamide', 'backring']

data = np.zeros((154, 7)).astype('bool')

i = 0
for line in lines:
    ligid = int(line.split()[0])
    filters = line.split(':')[1]

    if filters == '\n':
        i += 1
        continue

    for filt in filters.replace(' ', '').replace('\n', '').split(','):
        j = names_list.index(filt) 
        data[i, j] = True
    i += 1

df = pandas.DataFrame(data)
df.to_csv('test.csv')

