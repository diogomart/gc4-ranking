#!/usr/bin/env python

import numpy as np
import pandas

def parse_dat(fname):
    rmsds = []
    with open(fname) as f:
        first = True
        for line in f:
            if first == True: # skip header
                first = False
            else:
                _, y = line.split()
                rmsds.append(float(y))              
    return np.array(rmsds)

ligs = []
with open('LIGS') as f:
    for line in f:
        ligs.append(line.strip()) 

path = '/alpha/proj/grandchallenge4/from-Sukanya/stage2-adkVina/ligand/%s/%s'

data = {
    'ligand': [],
    'rmsd_lig_docked': [],
    'rmsd_lig_first': [],
    'rmsd_lig_last': [],
    'rmsd_align_docked': [],
    'rmsd_align_first': [],
    'rmsd_align_last': []
}

for lig in ligs:

    lig = 'BACE_%d' % int(lig)

    # ligand RMSD
    lig_docked = parse_dat(path % (lig, 'lig_docked.rmsd'))
    lig_first = parse_dat(path % (lig, 'lig_first.rmsd'))
    lig_last = parse_dat(path % (lig, 'lig_last.rmsd'))

    # protein selection RMSD
    align_docked = parse_dat(path % (lig, 'align_docked.rmsd'))
    align_first = parse_dat(path % (lig, 'align_first.rmsd'))
    align_last = parse_dat(path % (lig, 'align_last.rmsd'))

    # store averages
    data['ligand'].append(lig)
    data['rmsd_lig_docked'].append(np.mean(lig_docked))
    data['rmsd_lig_first'].append(np.mean(lig_first))
    data['rmsd_align_docked'].append(np.mean(align_docked))
    data['rmsd_align_first'].append(np.mean(align_first))
    data['rmsd_lig_last'].append(np.mean(lig_last[-40:]))
    data['rmsd_align_last'].append(np.mean(align_last[-40:]))

df = pandas.DataFrame(data)

