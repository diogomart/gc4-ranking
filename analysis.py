#!/usr/bin/env python

from matplotlib import pyplot as plt
import math
import sys
from scipy.stats import pearsonr, kendalltau, spearmanr

import pandas
import numpy as np

def plot_lignum_astext(df, x_column, y_column, fontdict={'color': 'red', 'size': 8}):

    for i, x, y in zip(df['lignum'], df[x_column], df[y_column]):
        plt.text(x, y, str(i),
                 transform=plt.gca().transData,
                 fontdict=fontdict)

def add_title(df, x_column, y_column):
    kt = kendalltau(df[x_column], df[y_column])
    pr = pearsonr(df[x_column], df[y_column])
    plt.title('Kendall tau = %.3f, pearsonr = %.3f' % (kt[0], pr[0]))

df = pandas.DataFrame.from_csv('data.csv')

xrayed = df[df['lignum'] <= 20]
noxray = df[df['lignum'] > 20]

def pairwise_bootstrap(x, y, metric, conf_intrval=0.95, B=10000):
    """
        input: x, and y arrays, metric function (e.g. kendalltau)
        metric returns coefficient and p-value (which is ignored)
        output: lower and upper bounds for metric coefficient
    """

    x = np.array(x)
    y = np.array(y)

    # size of input arrays
    if y.shape[0] != x.shape[0]:
        print "shape of x:", x.shape[0]
        print "shape of y:", y.shape[0]
        raise RuntimeError('x and y must have same size')
    n = x.shape[0]

    bootstrapped = []
    for i in xrange(B):
        indexes = np.random.choice(n, size=n, replace=True)
        x_bootstrap = x[indexes]
        y_bootstrap = y[indexes]
        coefficient, pvalue = metric(x_bootstrap, y_bootstrap)
        bootstrapped.append(coefficient)

    # sort bootstrapped coefficients
    bootstrapped = np.sort(bootstrapped)

    # limits of confidence interval e.g. for 99%: lower=0.005, upper=0.995 
    lower = (1. - conf_intrval) / 2.
    upper = 1.0 - lower

    # index of coefficient at desired confidence interval
    lower = int(B * lower)
    upper = int(B * upper)

    return bootstrapped[lower], bootstrapped[upper], np.std(bootstrapped)

def rsquared(x, y):
    r, pvalue = pearsonr(x, y)
    return r*r, pvalue

def printstats(x, y, ci=0.95):

    coef = kendalltau(x, y)[0]
    low, high, std = pairwise_bootstrap(x, y, kendalltau, conf_intrval=ci)
    print 'Kendall tau: %6.2f [%6.2f, %6.2f] CI=%.3f, std=%.2f' % (coef, low, high, ci, std)
    
    coef = pearsonr(x, y)[0]
    low, high, std = pairwise_bootstrap(x, y, pearsonr, conf_intrval=ci)
    print 'Pearson R:   %6.2f [%6.2f, %6.2f] CI=%.3f, std=%.2f' % (coef, low, high, ci, std)

    coef = spearmanr(x, y)[0]
    low, high, std = pairwise_bootstrap(x, y, spearmanr, conf_intrval=ci)
    print 'Spearman R:  %6.2f [%6.2f, %6.2f] CI=%.3f, std=%.2f' % (coef, low, high, ci, std)

    coef = rsquared(x, y)[0]
    low, high, std = pairwise_bootstrap(x, y, rsquared, conf_intrval=ci)
    print 'R squared:   %6.2f [%6.2f, %6.2f] CI=%.3f, std=%.2f' % (coef, low, high, ci, std)

def foo2(df, mask, metric):
    print 'docked: %8.3f (p-value: %6.4f)' % metric(df[mask].docked, df[mask].exprmt)
    print 'mmgbsa: %8.3f (p-value: %6.4f)' % metric(df[mask].mmgbsa, df[mask].exprmt)
    print 'eachot: %8.3f (p-value: %6.4f)' % metric(df[mask].mmgbsa, df[mask].docked)

def foo(df, mask, metric):

    coef = metric(df[mask].docked, df[mask].exprmt)[0]
    _, _, std = pairwise_bootstrap(df[mask].docked, df[mask].exprmt, metric)
    print 'docked: %8.3f (std: %5.3f)' % (coef, std)

    coef = metric(df[mask].mmgbsa, df[mask].exprmt)[0]
    _, _, std = pairwise_bootstrap(df[mask].mmgbsa, df[mask].exprmt, metric)
    print 'mmgbsa: %8.3f (std: %5.3f)' % (coef, std)

    coef = metric(df[mask].mmgbsa, df[mask].docked)[0]
    _, _, std = pairwise_bootstrap(df[mask].docked, df[mask].mmgbsa, metric)
    print 'eachot: %8.3f (std: %5.3f)' % (coef, std)

def kt(x, y, metric=kendalltau):
    value = metric(x, y)[0]
    low, high, std = pairwise_bootstrap(x, y, metric, B=1000)
    print '%8.3f (%8.3f) [%.3f --- %.3f]' % (value, std, low, high)
    return

exprmt = np.array(df.exprmt)
mmgbsa = np.array(df.mmgbsa)
re2wf3 = np.array(df.re2wf3c)

re2wf3all = np.array(df.mmgbsa)
mask = np.array(np.isnan(df.re2wf3c) == False)
re2wf3all[mask] = re2wf3[mask] 

docked = np.array(df.docked)
docked[np.array(df.rec == '4EWO')] = df[df.rec == '4EWO'].dock2wf3

print 'DOCKED (2WF3, n=154)'
for metric in [kendalltau, pearsonr, spearmanr, rsquared]:
    coef = metric(exprmt, docked)[0]
    _, _, std = pairwise_bootstrap(exprmt, docked, metric)
    sys.stdout.write('   %6.2f (%6.2f)' % (coef, std))
    sys.stdout.flush()


print 'MMGBSA (2WF3, n=154)'
for metric in [kendalltau, pearsonr, spearmanr, rsquared]:
    coef = metric(exprmt, re2wf3all)[0]
    _, _, std = pairwise_bootstrap(exprmt, re2wf3all, metric)
    sys.stdout.write('   %6.4f (%6.4f)' % (coef, std))
    sys.stdout.flush()


ash32 = np.array(df.ash32)

ash32all = np.array(df.mmgbsa)
mask = np.array(df.rec == '4EWO')

ash32all[mask] = ash32[mask]

kt(mmgbsa, exprmt)
kt(ash32all, exprmt)


print 'MMGBSA (4EWO single prot, n=154)'
for metric in [kendalltau, pearsonr, spearmanr, rsquared]:
    coef = metric(exprmt, ash32all)[0]
    _, _, std = pairwise_bootstrap(exprmt, ash32all, metric)
    sys.stdout.write('   %6.2f (%6.2f)' % (coef, std))
    sys.stdout.flush()


# print stats by receptor
for mask in [np.array(np.isnan(df.re2wf3)) == False, df.rec=='4EWO', df.rec=='2WF3', df.rec=='2B8V', df.rec=='2P4J']:
    print '\n'
    for predicted in ['docked', 'mmgbsa', 'ash32', 're2wf3']:
        sys.stdout.write(predicted)
        sys.stdout.flush()
        for metric in [kendalltau, pearsonr, spearmanr, rsquared]:
            x = df[mask][predicted]
            y = df[mask]['exprmt']
            coef = metric(x, y)[0]
            _, _, std = pairwise_bootstrap(x, y, metric)
            sys.stdout.write('   %6.2f (%6.2f)' % (coef, std))
            sys.stdout.flush()
        print ''
    print ''
    break




raise

print '\nA'
foo(df, df.A, kendalltau)
print '\nB'
foo(df, df.B, kendalltau)
print '\nC'
foo(df, df.C, kendalltau)
print '\nD'
foo(df, df.D, kendalltau)
print '\nE'
foo(df, df.E, kendalltau)
print '\nF'
foo(df, df.F, kendalltau)
print '\nG'
foo(df, df.G, kendalltau)

alll = df.exprmt < float('inf')
cation = df.charge == 1
neutral = df.charge == 0

alt_table = True

if alt_table:

    for mask in [alll, neutral, cation]:
        print '\n'
        for predicted in ['docked', 'mmgbsa']:
            sys.stdout.write(predicted)
            sys.stdout.flush()
            for metric in [kendalltau, pearsonr, spearmanr, rsquared]:
                x = df[mask][predicted]
                y = df[mask]['exprmt']
                coef = metric(x, y)[0]
                _, _, std = pairwise_bootstrap(x, y, metric)
                sys.stdout.write('   %6.2f (%6.2f)' % (coef, std))
                sys.stdout.flush()
            print ''
        print ''

else:

    for metric in [kendalltau, pearsonr, spearmanr, rsquared]:
        print metric
        for predicted in ['docked', 'mmgbsa']:
            sys.stdout.write(predicted)
            sys.stdout.flush() 
            for mask in [alll, neutral, cation]:
                x = df[mask][predicted]
                y = df[mask]['exprmt']
                coef = metric(x, y)[0]
                _, _, std = pairwise_bootstrap(x, y, metric)
                sys.stdout.write('   %6.2f (%6.2f)' % (coef, std))
                sys.stdout.flush()
            print ''
        print ''

raise


print 'Experiment vs docked, CHARGE ZERO'
printstats(df[df.charge == 0].exprmt, df[df.charge == 0].docked)
print ''

print 'Experiment vs MMGBSA, CHARGE ZERO'
printstats(df[df.charge == 0].exprmt, df[df.charge == 0].mmgbsa)
print ''

print 'Experiment vs docked, CHARGE +1'
printstats(df[df.charge == 1].exprmt, df[df.charge == 1].docked)
print ''

print 'Experiment vs MMGBSA, CHARGE +1'
printstats(df[df.charge == 1].exprmt, df[df.charge == 1].mmgbsa)
print ''


# exprmt vs mmgbsa
plt.clf()
plt.plot(xrayed['exprmt'], xrayed['mmgbsa'], '.g')
plt.plot(noxray['exprmt'], noxray['mmgbsa'], '.k', mfc='none')
plt.xlabel('log$_{10}$(affinity)')
plt.ylabel('MM-GBSA energy')
plot_lignum_astext(xrayed, 'exprmt', 'mmgbsa')
plot_lignum_astext(noxray, 'exprmt', 'mmgbsa', fontdict={'color': 'black', 'size': 6})
add_title(df, 'exprmt', 'mmgbsa')
plt.savefig('exprmt-vs-mmgbsa.png', dpi=300)

print 'Experiment vs MM-GBSA'
printstats(df.exprmt, df.mmgbsa)
print ''

# exprmt vs docked
plt.clf()
plt.plot(xrayed['exprmt'], xrayed['docked'], '.g')
plt.plot(noxray['exprmt'], noxray['docked'], '.k', mfc='none')
plt.xlabel('log$_{10}$(affinity)')
plt.ylabel('autodock energy')
plot_lignum_astext(xrayed, 'exprmt', 'docked')
plot_lignum_astext(noxray, 'exprmt', 'docked', fontdict={'color': 'black', 'size': 6})
add_title(df, 'exprmt', 'docked')
plt.savefig('exprmt-vs-docked.png', dpi=300)

print 'Experiment vs docked'
printstats(df.exprmt, df.docked)
print ''

# docked vs mmgbsa
plt.clf()
plt.plot(xrayed['docked'], xrayed['mmgbsa'], '.g')
plt.plot(noxray['docked'], noxray['mmgbsa'], '.k', mfc='none')
plt.xlabel('autodock energy')
plt.ylabel('MM-GBSA energy')
plot_lignum_astext(xrayed, 'docked', 'mmgbsa')
plot_lignum_astext(noxray, 'docked', 'mmgbsa', fontdict={'color': 'black', 'size': 6})
add_title(df, 'docked', 'mmgbsa')
plt.savefig('docked-vs-mmgbsa.png', dpi=300)

print 'Docked vs MM-GBSA'
printstats(df.docked, df.mmgbsa)
print ''

