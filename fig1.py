#!/usr/bin/env python

from matplotlib import pyplot as plt
from scipy.stats import pearsonr, kendalltau, spearmanr

import pandas
import numpy as np
import matplotlib

df = pandas.DataFrame.from_csv('data.csv')

# convert experimental values from log(IC50(uM)) to pIC50 (M)
df.exprmt = -df.exprmt + 6

exprmt = np.array(df.exprmt)
mmgbsa = np.array(df.mmgbsa)
docked = np.array(df.docked)
re2wf3 = np.array(df.re2wf3)

re2wf3all = np.array(df.mmgbsa)
mask1 = np.array(np.isnan(df.re2wf3) == False)
re2wf3all[mask1] = re2wf3[mask1] 



ash32 = np.array(df.ash32)

ash32all = np.array(df.mmgbsa)
mask = np.array(df.rec == '4EWO')

ash32all[mask] = ash32[mask]

# matplotlib
matplotlib.rcParams.update({'font.size': 6, 'axes.linewidth': 0.5, 'lines.markersize': 4})
fig, axs = plt.subplots(2, 2, sharex=True, sharey=False)
fig.set_size_inches(3.2, 3.2)

# exprmt vs docking
axs[0][0].plot(df[df.rec=='4EWO'].exprmt, df[df.rec=='4EWO'].docked, 'or', alpha=0.5, label='4EWO')
axs[0][0].plot(df[df.rec=='2WF3'].exprmt, df[df.rec=='2WF3'].docked, 'k^', alpha=0.5, label='2WF3')
axs[0][0].plot(df[df.rec=='2B8V'].exprmt, df[df.rec=='2B8V'].docked, 'gd', alpha=0.5, label='2B8V')
axs[0][0].plot(df[df.rec=='2P4J'].exprmt, df[df.rec=='2P4J'].docked, 'cs', alpha=0.5, label='2P4J')
axs[0][0].text(-0.25, 0.5, 'AutoDock4.2 Score', ha='center', va='center', rotation=90, transform=axs[0][0].transAxes)
axs[0][0].text(-0.25, 1.1, 'a', ha='center', va='center', transform=axs[0][0].transAxes, size=9)

# exprmt vs MM-GBSA
axs[0][1].plot(df[df.rec=='4EWO'].exprmt, df[df.rec=='4EWO'].mmgbsa, 'or', alpha=0.5) #, mfc='none')
axs[0][1].plot(df[df.rec=='2WF3'].exprmt, df[df.rec=='2WF3'].mmgbsa, 'k^', alpha=0.5) #, mfc='none')
axs[0][1].plot(df[df.rec=='2B8V'].exprmt, df[df.rec=='2B8V'].mmgbsa, 'gd', alpha=0.5) #, mfc='none')
axs[0][1].plot(df[df.rec=='2P4J'].exprmt, df[df.rec=='2P4J'].mmgbsa, 'cs', alpha=0.5) #, mfc='none')
axs[0][1].text(-0.25, 0.5, 'MM-GBSA Score', ha='center', va='center', rotation=90, transform=axs[0][1].transAxes)
axs[0][1].text(-0.25, 1.1, 'b', ha='center', va='center', transform=axs[0][1].transAxes, size=9)
axs[0][1].set_ylim(-75, -10)

# exprmt vs MMGBSA (4EWO single ASH)
axs[1][0].plot(df[df.rec=='4EWO'].exprmt, df[df.rec=='4EWO'].ash32,  'or', alpha=0.5) #, mfc='none')
axs[1][0].plot(df[df.rec=='2WF3'].exprmt, df[df.rec=='2WF3'].mmgbsa, 'k^', alpha=0.5) #, mfc='none')
axs[1][0].plot(df[df.rec=='2B8V'].exprmt, df[df.rec=='2B8V'].mmgbsa, 'gd', alpha=0.5) #, mfc='none')
axs[1][0].plot(df[df.rec=='2P4J'].exprmt, df[df.rec=='2P4J'].mmgbsa, 'cs', alpha=0.5) #, mfc='none')
axs[1][0].text(-0.25, 0.5, 'MM-GBSA Score', ha='center', va='center', rotation=90, transform=axs[1][0].transAxes)
axs[1][0].text(0.5, -0.2,  'pIC$_{50}$', ha='center', va='center', transform=axs[1][0].transAxes)
axs[1][0].text(-0.25, 1.1, 'c', ha='center', va='center', transform=axs[1][0].transAxes, size=9)
axs[1][0].set_ylim(-75, -10)

# exprmt vs MMGBSA (4EWO --> 2WF3)
mask1 = (df.rec == '4EWO') & (np.isnan(df.re2wf3)) # still 4EWO scores
mask2 = (np.isnan(df.re2wf3) == False) # re2wf3
axs[1][1].plot(df[mask1         ].exprmt, df[mask1         ].mmgbsa, 'or', alpha=0.5) #, mfc='none')
axs[1][1].plot(df[df.rec=='2WF3'].exprmt, df[df.rec=='2WF3'].mmgbsa, 'k^', alpha=0.5) #, mfc='none')
axs[1][1].plot(df[mask2         ].exprmt, df[mask2         ].re2wf3, 'k^', alpha=0.5) #, mfc='none')
axs[1][1].plot(df[df.rec=='2B8V'].exprmt, df[df.rec=='2B8V'].mmgbsa, 'gd', alpha=0.5) #, mfc='none')
axs[1][1].plot(df[df.rec=='2P4J'].exprmt, df[df.rec=='2P4J'].mmgbsa, 'cs', alpha=0.5) #, mfc='none')
axs[1][1].text(-0.25, 0.5, 'MM-GBSA Score', ha='center', va='center', rotation=90, transform=axs[1][1].transAxes)
axs[1][1].text(0.5, -0.2,  'pIC$_{50}$', ha='center', va='center', transform=axs[1][1].transAxes)
axs[1][1].text(-0.25, 1.1, 'd', ha='center', va='center', transform=axs[1][1].transAxes, size=9)
axs[1][1].set_ylim(-75, -10)

l = axs[0][0].legend(title='Protein conformation (PDB ID)', prop={'size': 6}, handletextpad=0.1,
              bbox_to_anchor=(0.0,1.18,2.4,0.2),
              loc="lower left",
              mode="expand",
              borderaxespad=0,
              ncol=4,
              numpoints=1)
l.get_frame().set_linewidth(0.6)
t = l.get_texts()
t[0].set_text('4EWO')
t[1].set_text('2WF3')
t[2].set_text('2B8V')
t[3].set_text('2P4J')



plt.subplots_adjust(right=0.98, wspace=0.4, hspace=0.15, left=0.12, top=0.85)
plt.savefig('f1.png', dpi=600)




